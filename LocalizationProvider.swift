//
//  LocalizationProvider.swift
//  Messenger
//
//  Created by Eugene Martinson on 19/02/2019.
//  Copyright © 2019 BTS Digital. All rights reserved.
//

import Foundation

final class DefaultsLocalizationPersister: LocalizationPersister {
    
    private let userDefaults: UserDefaults
    
    init(userDefaults: UserDefaults) {
        self.userDefaults = userDefaults
    }
    
    func save(_ locale: Locale) {
        UserDefaults.standard.set(locale.identifier, forKey: "Lang")
    }
    
    func load() -> Locale? {
        guard let lang = userDefaults.string(forKey: "Lang") else {
            return nil
        }
        return Locale(identifier: lang)
    }
}

protocol LocalizationPersister: AnyObject {
    func save(_ locale: Locale)
    func load() -> Locale?
}

public final class LocalizationProvider {
    static var localePersister: LocalizationPersister =
        DefaultsLocalizationPersister(userDefaults: UserDefaults.standard)
    
    static func bundle(for locale: String = "ru") -> Bundle {
        guard let path = Bundle.main.path(forResource: locale, ofType: "lproj"), let bundle = Bundle(path: path) else {
            fatalError("No \(locale) localization found")
        }
        return bundle
    }
    
    public static var mainBundle: Bundle = .main
    
    static var localizationBundle: Bundle {
        guard let path = mainBundle.path(forResource: locale.languageCode, ofType: "lproj"),
            let bundle = Bundle(path: path) else {
                return Bundle.main
                //fatalError("No \(locale.identifier) localization found")
        }
        return bundle
    }
    
    public static var locale: Locale {
        get {
            return localePersister.load() ?? Locale(identifier: "ru")
        }
        set {
            localePersister.save(newValue)
        }
    }
}
