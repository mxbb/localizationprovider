Pod::Spec.new do |s|
  s.name = 'LocalizationProvider'
  s.version = '0.0.1'
  s.summary = 'LocalizationProvider'
  s.homepage = 'www.btsd.ru'
  s.license = { :type => 'LLL', :file => 'LICENSE' }
  s.author = { 'LocalizationProvider' => 'www.btsd.ru' }
  s.platform = :ios, '9.0'
  s.requires_arc = true

  s.source_files = '*.swift'
  s.resources = ''
  s.source = { :path => '.' }

end


